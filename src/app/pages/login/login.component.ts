import {Component, OnInit} from '@angular/core';
import {WebSocketsService} from '../../services/web-sockets.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  name: string = '';

  constructor(private wsService: WebSocketsService,
              private route: Router) {
  }

  ngOnInit(): void {
  }

  enterUser(): void {
    if (this.name.trim().length <= 0) {
      return;
    }
    this.wsService.loginWS(this.name).then(() => {
      this.route.navigateByUrl('/messages');
    });
  }


}
