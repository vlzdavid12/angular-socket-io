import { Component, OnInit } from '@angular/core';
import {WebSocketsService} from '../../services/web-sockets.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {


  constructor(public wsService: WebSocketsService) { }

  ngOnInit(): void {
  }

  exitApp(){
    this.wsService.logoutWS();
  }

}
