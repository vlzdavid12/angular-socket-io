import { Component, OnInit } from '@angular/core';
import {WebSocketsService} from '../../services/web-sockets.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styles: [
  ]
})
export class FooterComponent implements OnInit {

  constructor(public wsService: WebSocketsService) { }

  ngOnInit(): void {
  }

}
