import { Component } from '@angular/core';
import {WebSocketsService} from '../../services/web-sockets.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent  {

  constructor(public wsService: WebSocketsService) { }


}
