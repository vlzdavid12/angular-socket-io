import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {environment} from '../environments/environment';

// Connect Socket Io
const config: SocketIoConfig = {
  url: environment.wsUrl,
  options: {}
};

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';
import {FormsModule} from '@angular/forms';
import { ListUserComponent } from './components/list-user/list-user.component';
import { LoginComponent } from './pages/login/login.component';
import { MessagesComponent } from './pages/messages/messages.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    ListUserComponent,
    LoginComponent,
    MessagesComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        SocketIoModule.forRoot(config),
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
