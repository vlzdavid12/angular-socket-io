import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {WebSocketsService} from '../services/web-sockets.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements  CanActivate{

  constructor(private wsService: WebSocketsService,
              private route: Router) { }

  canActivate(): boolean  {
    if (this.wsService.getUser()){
      return true;
    } else {
      this.route.navigateByUrl('/');
      return false;
    }
  }


}
