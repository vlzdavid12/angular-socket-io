import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

  txt = '';
  msgSubscription!: Subscription;
  messages: any[] = [];

  @ViewChild('chatElement') element!: ElementRef;

  constructor(private chatService: ChatService) {
  }

  ngOnInit(): void {
    this.msgSubscription = this.chatService.getMessages()
      .subscribe(msg => {
        console.log(msg)
        this.messages.push(msg);
        setTimeout(() => {
          this.element.nativeElement.scrollTop = this.element.nativeElement.scrollHeight;
        }, 50);
      });
  }

  sendTXT(): void {
    if (this.txt.length <= 0) return;
    this.chatService.sendMessages(this.txt);
    this.txt = '';
  }

  ngOnDestroy(): void {

    this.msgSubscription.unsubscribe();

  }

}
