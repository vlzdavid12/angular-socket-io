import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  usersActivesObs!: Observable<any>;

  constructor(public chatService: ChatService) { }

  ngOnInit(): void {
    this.usersActivesObs = this.chatService.getUserActives();

    // Emit Get User
    this.chatService.emitUsersActives();
  }

}
