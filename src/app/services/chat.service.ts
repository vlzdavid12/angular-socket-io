import {Injectable} from '@angular/core';
import {WebSocketsService} from './web-sockets.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private wsService: WebSocketsService) {
  }

  sendMessages(txt: string): void {
    const payload = {
      of: this.wsService.getUser().name,
      body: txt
    };

    this.wsService.emit('message', payload);
  }

  getMessages(): Observable<any> {
    return this.wsService.listen('new-message');
  }


  getMessagesPrivate(): Observable<any> {
    return this.wsService.listen('message-private');
  }

  getUserActives(): Observable<any>  {
    return this.wsService.listen( 'actives-users');
  }

  emitUsersActives(): void{
    this.wsService.emit('get_users');
  }
}
