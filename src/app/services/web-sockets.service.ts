import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {Observable} from 'rxjs';
import {User} from '../classes/usuario';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WebSocketsService {

  public socketStatus: boolean = false;
  public user: any = null;

  constructor(private socket: Socket,
              private route: Router) {
    this.checkStatus();
    this.loaderStorage();
  }

  checkStatus(): void {
    this.socket.on('connect', () => {
      console.log('Connect to Client!');
      this.socketStatus = true;
    });

    this.socket.on('disconnect', () => {
      console.log('Disconnect to server!');
      this.socketStatus = false;
    });
  }


  // Send Messages
  // tslint:disable-next-line:ban-types
  emit(event: string, payload?: any, callback?: Function): void {
    // emit('Event', payload, callback)
    this.socket.emit(event, payload, callback);
  }

  // Received Messages
  listen(event: string): Observable<any> {
    return this.socket.fromEvent(event);
  }

  loginWS(name: string): any {
    return new Promise<void>((resolve, reject) => {
      this.emit('config_user', {name}, (resp: any) => {
        this.user = new User(name);
        this.saveStorage();
        resolve();
      });
    });
  }

  logoutWS(): void{
    this.user = null;
    localStorage.removeItem('user');
    const payload = {
      name: 'not-name'
    }
    this.emit('config_user', payload, (res: any) => console.log(res));
    this.route.navigateByUrl('/');
  }

  getUser(): User{
    return this.user;
  }

  saveStorage(): void{
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  loaderStorage(): void{
    if (localStorage.getItem('user')){
      const userStorage: any  = localStorage.getItem('user');
      this.user = JSON.parse( userStorage );
      // Loader Important of APP
      this.loginWS(this.user.name);
    }
  }

}
